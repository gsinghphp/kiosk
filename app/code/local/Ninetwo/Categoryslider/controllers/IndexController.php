<?php
class Ninetwo_Categoryslider_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$categoryId = $this->getRequest()->getParam('category_id');
		$mainCatId = $this->getRequest()->getParam('main_cat_id');
		$selectId = $this->getRequest()->getParam('selectedarr');
		 
		
		$sessionCatIdsArray = Mage::getModel('catalog/session')->getCatArraySession();
		
		$sessionCatIdsArray[$mainCatId] = $categoryId;
		Mage::getModel('catalog/session')->setCatArraySession($sessionCatIdsArray);
		
		Mage::register('selectedarr', explode(',',$selectId)); 
		
		$this->getResponse()->setBody($this->getLayout()->createBlock('categoryslider/categoryslider')->setTemplate('categoryslider/categoryslider.phtml')->setPost($this->getRequest()->getParam('all_child_cat'))->toHtml());
    }
	
	public function resetsessionAction(){
		Mage::getModel('catalog/session')->unsCatArraySession();
	}
	
	public function autocompleteproductsAction(){
	
		if($this->getRequest()->getParam('term') != ""){
			$block = Mage::app()->getLayout()->createBlock('categoryslider/categoryslider')->getProductautosearch();
		}
	
	}
	
	 
}
