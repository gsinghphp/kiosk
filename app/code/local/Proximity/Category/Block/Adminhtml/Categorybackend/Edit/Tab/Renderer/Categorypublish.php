<?php

class Proximity_Category_Block_Adminhtml_Categorybackend_Edit_Tab_Renderer_Categorypublish extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        if($row->getPublish())
            return "Yes";
        else 
            return "No";
    }

}
