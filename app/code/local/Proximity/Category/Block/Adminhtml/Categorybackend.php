<?php

class Proximity_Category_Block_Adminhtml_Categorybackend extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {

        $this->_controller = 'adminhtml_categorybackend';
        $this->_blockGroup = 'category';
        $this->_headerText = 'Manage Category';
        $this->_addButtonLabel = Mage::helper('category')->__('Add a Category');

        parent::__construct();
    }

    protected function _prepareLayout() {
        $this->setChild('grid', $this->getLayout()->createBlock($this->_blockGroup . '/' . $this->_controller . '_grid', $this->_controller . '.grid')->setSaveParametersInSession(true));
        return parent::_prepareLayout();
    }

}
