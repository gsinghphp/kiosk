<?php

class Proximity_Category_Model_Category extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init("category/category");
    }

    public function getPublishCategories() {

        $collection = $this->getCollection()->addFieldToFilter( 'publish', '1' );
        $categories = $collection->getData();
        $data = array();
        foreach ($categories as $category) {
            $data[$category['category_id']] = $category['title'];
        }

        return $data;
    }

    public function getCategoryName($catId) {
        $data = $this->load($catId);
        return $data->getData('title');
    }

}
