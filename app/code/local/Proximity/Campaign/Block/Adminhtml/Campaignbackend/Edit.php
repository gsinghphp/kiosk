<?php

class Proximity_Campaign_Block_Adminhtml_Campaignbackend_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        
        $this->_objectId = 'campaign_id';
        $this->_blockGroup = 'campaign';
        $this->_controller = 'adminhtml_campaignbackend';

        parent::__construct();
        
        $this->_updateButton('save', 'label', 'Save Campaign');
        $this->_updateButton('delete', 'label', 'Delete Campaign');
    }

    public function getHeaderText() {
        return 'Create/Update Campaign';
    }

}
