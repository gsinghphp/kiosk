<?php

$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT
ALTER TABLE `proximity_client`
	CHANGE COLUMN `beacon` `beacon_id` VARCHAR(255) NOT NULL AFTER `lon`;
SQLTEXT;

$installer->run($sql);
$installer->endSetup();
