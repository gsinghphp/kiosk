<?php

class Proximity_Beacon_Block_Adminhtml_Beaconbackend_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function _construct() {

        $this->_blockGroup = 'beacon';
        $this->_controller = 'adminhtml_beaconbackend';
        $this->_objectId = 'beacon_id';

        parent::_construct();

        $this->_updateButton('save', 'label', 'Save Beacon');
        $this->_updateButton('delete', 'label', 'Delete Beacon');
    }

    public function getHeaderText() {
        return 'Create/Update Beacon';
    }

}
