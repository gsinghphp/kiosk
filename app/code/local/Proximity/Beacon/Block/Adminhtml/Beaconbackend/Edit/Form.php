<?php

class Proximity_Beacon_Block_Adminhtml_Beaconbackend_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    public function _prepareLayout() {

        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
    }

    public function _prepareForm() {

        $form = new Varien_Data_Form(
                array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('beacon_id' => $this->getRequest()->getParam('beacon_id'))
            ),
            'method' => 'post',
                )
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        parent::_prepareForm();
    }

}
