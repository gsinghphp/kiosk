<?php

class Proximity_Beacon_Block_Adminhtml_Beaconbackend_Edit_Tab_Renderer_Beaconstatus extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $status = $row->getStatus();
        if ($status)
            return "Enable";
        else
            return "Disable";
    }

}
