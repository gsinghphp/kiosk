<?php

class Proximity_Beacon_Block_Adminhtml_Beaconbackend_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function _construct() {
        
        parent::_construct();
        
        $this->setId('proximity_beacon_grid');
        $this->setDefaultSort('beacon_id');
        $this->setDefaultDir('ASC');
        
        $this->setSaveParametersInSession(true);
    }
    
    public function _prepareCollection() {
        
        $collection = Mage::getModel('beacon/beacon')->getCollection();
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }
    
    public function _prepareColumns() {
        
        $this->addColumn('beacon_id', array(
            'header' => 'ID',
            'align' => 'right',
            'width' => '50px',
            'index' => 'beacon_id',
        ));
        
        $this->addColumn('beacon_name', array(
            'header' => 'Beacon Name',
            'align' => 'left',
            'index' => 'beacon_name',
        ));
        
        $this->addColumn('status', array(
            'header' => 'Beacon Status',
            'align' => 'left',
            'index' => 'status',
            'renderer'=>'Proximity_Beacon_Block_Adminhtml_Beaconbackend_Edit_Tab_Renderer_Beaconstatus',
        ));
        
        $this->addColumn('description', array(
            'header' => 'Description',
            'align' => 'left',
            'index' => 'description',
            'renderer'=>'Proximity_Beacon_Block_Adminhtml_Beaconbackend_Edit_Tab_Renderer_Beacondesc',
        ));
        
        $this->addColumn('beacon_number', array(
            'header' => 'Beacon Number',
            'align' => 'left',
            'index' => 'beacon_number',
        ));
        
        $this->addColumn('uid', array(
            'header' => 'UUID',
            'align' => 'left',
            'index' => 'uid',
        ));
        
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('beacon')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('beacon')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'beacon_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('beacon_id' => $row->getId()));
    }
}
