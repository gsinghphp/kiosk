<?php

class Proximity_Beacon_Block_Adminhtml_Beaconbackend extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function _construct() {

        $this->_controller = 'adminhtml_beaconbackend';
        $this->_blockGroup = 'beacon';
        $this->_headerText = 'Manage Beacon';

        $this->_addButtonLabel = 'Create Beacon';

        parent::_construct();
    }

    public function _prepareLayout() {

        $type = $this->_blockGroup . '/' . $this->_controller . '_grid';
        $name = $this->_controller . '.grid';

        $alias = 'grid';
        $block = $this->getLayout()->createBlock($type, $name)->setSaveParametersInSession(true);

        $this->setChild($alias, $block);
        return parent::_prepareLayout();
    }

}
