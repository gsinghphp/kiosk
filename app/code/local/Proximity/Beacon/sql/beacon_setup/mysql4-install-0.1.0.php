<?php

$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT
CREATE TABLE `proximity_beacon` (
	`beacon_id` INT(50) NOT NULL AUTO_INCREMENT,
	`beacon_name` VARCHAR(50) NOT NULL,
	`status` TINYINT(5) NOT NULL,
	`description` TEXT NOT NULL,
	`beacon_number` INT(50) NOT NULL,
	`uid` VARCHAR(50) NOT NULL,
	`major_number` INT(50) NOT NULL,
	`minor_number` INT(50) NOT NULL,
	`proximity` VARCHAR(50) NOT NULL,
	`refresh_rate` INT(20) NOT NULL,
	`message` TEXT NOT NULL,
	`url` VARCHAR(50) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	PRIMARY KEY (`beacon_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
		
SQLTEXT;

$installer->run($sql);
$installer->endSetup();
