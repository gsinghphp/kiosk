<?php

$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT
ALTER TABLE `proximity_beacon`
	ADD COLUMN `page_id` INT(50) NULL AFTER `url`;
SQLTEXT;

$installer->run($sql);
$installer->endSetup();
