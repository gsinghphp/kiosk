<?php
class Seasia_Productautosearch_Block_Productautosearch extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    } 
    
	public function getProductautosearch()
	{
		
		$term = $this->getRequest()->getParam('term');
		$query = Mage::getModel('catalogsearch/query')->setQueryText($term)->prepare();
		$fulltextResource = Mage::getResourceModel('catalogsearch/fulltext')->prepareResult(
			Mage::getModel('catalogsearch/fulltext'),
			$term,
			$query
		);

		$collection = Mage::getResourceModel('catalog/product_collection');
		$collection->getSelect()->joinInner(
			array('search_result' => $collection->getTable('catalogsearch/result')),
			$collection->getConnection()->quoteInto(
				'search_result.product_id=e.entity_id AND search_result.query_id=?',
				$query->getId()
			),
			array('relevance' => 'relevance')
		);
		$productIds = array();
		$productIds = $collection->getAllIds(); // as per Amit Bera s' comment
		return $productIds;
		
		
	}
}