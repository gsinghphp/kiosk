<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
class Seasia_Promocode_Adminhtml_PromocodeController extends Mage_Adminhtml_Controller_action {
	
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('promocode/manage')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	} 
	
	public function indexAction() {
		$this->_initAction();
		$this->renderLayout();
	}
	
	/*public function gridAction(){
		$this->loadLayout();
		$this->renderLayout();
	}*/
	
	public function newAction() {
		$this->_forward('edit');
	}
	
	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('promocode/promocode')->load($id);
	
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
			    //$timer = explode(':',$data['timer']);
			
				//$data['timer'] = $timer;
				$model->setData($data);
			}
	
			Mage::register('promocode_data', $model);
	
			$this->loadLayout();
			$this->_setActiveMenu('promocode/items');
	
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
	
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
	
			$this->_addContent($this->getLayout()->createBlock('promocode/adminhtml_promocode_edit'))
			->_addLeft($this->getLayout()->createBlock('promocode/adminhtml_promocode_edit_tabs'));
	
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('promocode')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
	
	
	public function saveAction() 
	{
		$model = Mage::getModel('promocode/promocode');
		if ($data = $this->getRequest()->getPost()) 
		{
			
			
			$model->setData($data)
			->setId($this->getRequest()->getParam('id'));
	
			try 
			{
				//multiple coupons should not be assigned to beacon
				$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
				$connection->beginTransaction();
				
				$select = $connection->select()
				->from('seasia_beacon_promocodes', array('*'))
		   ->where('beacon_id='.$data['beacon_id'].' AND proximity="'.$data['proximity'].'" AND coupon_id ='.$data['coupon_id']);
				$rowsArray = $connection->fetchAll($select);						
				$connection->commit();
				
				if(!empty($rowsArray) && $this->getRequest()->getParam('id') == "")
				//if(!empty($rowsArray))
				{   
					Mage::getSingleton('adminhtml/session')->addError("Coupon and Proximity combination already exits for this beacon.");
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				
				
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) 
				{
					$model->setCreatedTime(now())
					->setUpdateTime(now());
				} 
				else 
				{
					$model->setUpdateTime(now());
				}
				
				//$model->setTemp_range($data['temp_range']);
				
				
				//if coupon is assigned also save promo image if not empty
				if(!empty($_FILES['promo_image']['name']))
				{
					try 
					{
						$uploader = new Varien_File_Uploader('promo_image');
						$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); // or pdf or anything
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						$path = Mage::getBaseDir('media') . "/promo_images/";
						$file_name = round(microtime(true))."_".$_FILES['promo_image']['name'];
						
						$uploader->save($path, $file_name);
						//$data['promo_image'] =$path. $file_name;
						$data['promo_image'] =$file_name;
						$model->setPromo_image($file_name);
					}
					catch(Exception $e) 
					{
						Mage::throwException($e->getMessage());
						return $this;
					}
				}
// 				$temp_range = explode("and",$data['temp_range']);
				
			//set min max temp
// 				$model->setMin_temp($temp_range[0]);
// 				$model->setMax_temp($temp_range[1]);

		

	
		/*	$timer = $data['test'];
			$sched_time = $timer[0] . ':' . $timer[1] . ':' . $timer[2]; //HH:MM:SS //HH:MM:SS
			$data['test'] = $sched_time;
			*/
			
			$data['timer'] = $data['timer_hours'].':'.$data['timer_minutes'];
			
			if($this->getRequest()->getParam('id') != ""){
				$data['id'] = $this->getRequest()->getParam('id');
				}
			

			$data['updated_at'] = date('Y-m-d H:i:s');		
			$model->setData($data); 
		
			
		
			
			$model->save();
			
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('promocode')->__('Coupon was successfully assigned'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
	
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('promocode')->__('Unable to find item to save'));
		$this->_redirect('*/*/');
	}
	
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('promocode/promocode');
					
				$model->setId($this->getRequest()->getParam('id'))
				->delete();
	
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}
	
	/**
	 * mass delete promocode - action
	 *
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function massDeleteAction()
	{
		$promocodeIds = $this->getRequest()->getParam('promocode');
		if (!is_array($promocodeIds)) {
			Mage::getSingleton('adminhtml/session')->addError(
					Mage::helper('promocode')->__('Please select promocode to delete.')
			);
		} else {
			try {
				foreach ($promocodeIds as $promocodeId) {
					$promocode = Mage::getModel('promocode/promocode');
					$promocode->setId($promocodeId)->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
						Mage::helper('promocode')->__('Total of %d promocode were successfully deleted.', count($promocodeIds))
				);
			} catch (Mage_Core_Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(
						Mage::helper('promocode')->__('There was an error deleting promocode.')
				);
				Mage::logException($e);
			}
		}
		$this->_redirect('*/*/index');
	}
	
}
