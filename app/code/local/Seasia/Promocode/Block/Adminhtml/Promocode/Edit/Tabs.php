<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
class Seasia_Promocode_Block_Adminhtml_Promocode_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('promocode_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('promocode')->__('Promocode Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('promocode')->__('General'),
          'title'     => Mage::helper('promocode')->__('General'),
          'content'   => $this->getLayout()->createBlock('promocode/adminhtml_promocode_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}