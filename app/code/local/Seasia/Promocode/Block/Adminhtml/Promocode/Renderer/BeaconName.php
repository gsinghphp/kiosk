<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
class Seasia_Promocode_Block_Adminhtml_Promocode_Renderer_BeaconName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{

	
	public function render(Varien_Object $row)
    {
		$records = $row->getData();
    	$beaconId = $records['beacon_id'];
    	
    	//get name of beacon acc to id
    	$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
    	$sql        = "Select name as beacon_name from seasia_beacon_beacon WHERE entity_id = ".$beaconId;
    	$rows       = $connection->fetchAll($sql);
		
    	return $rows[0]['beacon_name'];
    }
}