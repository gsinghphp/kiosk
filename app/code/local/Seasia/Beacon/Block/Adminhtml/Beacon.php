<?php
/**
 * Seasia_Beacon extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Seasia
 * @package        Seasia_Beacon
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Beacon admin block
 *
 * @category    Seasia
 * @package     Seasia_Beacon
 * @author      Ultimate Module Creator
 */
class Seasia_Beacon_Block_Adminhtml_Beacon extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_beacon';
        $this->_blockGroup         = 'seasia_beacon';
        parent::__construct();
        $this->_headerText         = Mage::helper('seasia_beacon')->__('Beacon');
        $this->_updateButton('add', 'label', Mage::helper('seasia_beacon')->__('Add Beacon'));

    }
}
