<?php
/**
 * Seasia_Beacon extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Seasia
 * @package        Seasia_Beacon
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Beacon admin grid block
 *
 * @category    Seasia
 * @package     Seasia_Beacon
 * @author      Ultimate Module Creator
 */
class Seasia_Beacon_Block_Adminhtml_Beacon_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('beaconGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Seasia_Beacon_Block_Adminhtml_Beacon_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('seasia_beacon/beacon')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Seasia_Beacon_Block_Adminhtml_Beacon_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('seasia_beacon')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'name',
            array(
                'header'    => Mage::helper('seasia_beacon')->__('name'),
                'align'     => 'left',
                'index'     => 'name',
            )
        );
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('seasia_beacon')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    '1' => Mage::helper('seasia_beacon')->__('Enabled'),
                    '0' => Mage::helper('seasia_beacon')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'description',
            array(
                'header' => Mage::helper('seasia_beacon')->__('description'),
                'index'  => 'description',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'beacon_number',
            array(
                'header' => Mage::helper('seasia_beacon')->__('Beacon Number'),
                'index'  => 'beacon_number',
                'type'=> 'number',

            )
        );
        $this->addColumn(
            'uuid',
            array(
                'header' => Mage::helper('seasia_beacon')->__('UUID'),
                'index'  => 'uuid',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'major',
            array(
                'header' => Mage::helper('seasia_beacon')->__('Major'),
                'index'  => 'major',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'minor',
            array(
                'header' => Mage::helper('seasia_beacon')->__('Minor'),
                'index'  => 'minor',
                'type'=> 'text',

            )
        );
        /*$this->addColumn(
            'range',
            array(
                'header' => Mage::helper('seasia_beacon')->__('Proximity'),
                'index'  => 'proximity',
                'type'  => 'options',
                'options' => Mage::helper('seasia_beacon')->convertOptions(
                    Mage::getModel('seasia_beacon/beacon_attribute_source_proximity')->getAllOptions(false)
                )

            )
        );*/
        $this->addColumn(
        		'refresh_rate',
        		array(
        				'header' => Mage::helper('seasia_beacon')->__('Refresh Rate(mins)'),
        				'index'  => 'refresh_rate',
        				'type'=> 'text',
        
        		)
        );
        $this->addColumn(
        		'message',
        		array(
        				'header' => Mage::helper('seasia_beacon')->__('Message'),
        				'index'  => 'message',
        				'type'=> 'text',
        
        		)
        );
       /* $this->addColumn(
        		'category',
        		array(
        				'header' => Mage::helper('seasia_beacon')->__('Category'),
        				'index'  => 'category',
        				'type'=> 'text',
        
        		)
        );
        $this->addColumn(
        		'vendor',
        		array(
        				'header' => Mage::helper('seasia_beacon')->__('Vendor'),
        				'index'  => 'vendor',
        				'type'=> 'text',
        
        		)
        );*/
        
        if (!Mage::app()->isSingleStoreMode() && !$this->_isExport) {
            $this->addColumn(
                'store_id',
                array(
                    'header'     => Mage::helper('seasia_beacon')->__('Store Views'),
                    'index'      => 'store_id',
                    'type'       => 'store',
                    'store_all'  => true,
                    'store_view' => true,
                    'sortable'   => false,
                    'filter_condition_callback'=> array($this, '_filterStoreCondition'),
                )
            );
        }
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('seasia_beacon')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('seasia_beacon')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('seasia_beacon')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('seasia_beacon')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('seasia_beacon')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('seasia_beacon')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('seasia_beacon')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Seasia_Beacon_Block_Adminhtml_Beacon_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('beacon');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('seasia_beacon')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('seasia_beacon')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('seasia_beacon')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('seasia_beacon')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('seasia_beacon')->__('Enabled'),
                            '0' => Mage::helper('seasia_beacon')->__('Disabled'),
                        )
                    )
                )
            )
        );
        $this->getMassactionBlock()->addItem(
            'range',
            array(
                'label'      => Mage::helper('seasia_beacon')->__('Change Range'),
                'url'        => $this->getUrl('*/*/massRange', array('_current'=>true)),
                'additional' => array(
                    'flag_range' => array(
                        'name'   => 'flag_range',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('seasia_beacon')->__('Range'),
                        'values' => Mage::getModel('seasia_beacon/beacon_attribute_source_proximity')
                            ->getAllOptions(true),

                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Seasia_Beacon_Model_Beacon
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Seasia_Beacon_Block_Adminhtml_Beacon_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * filter store column
     *
     * @access protected
     * @param Seasia_Beacon_Model_Resource_Beacon_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return Seasia_Beacon_Block_Adminhtml_Beacon_Grid
     * @author Ultimate Module Creator
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $collection->addStoreFilter($value);
        return $this;
    }
}
