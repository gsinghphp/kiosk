<?php
/**
 * Seasia_Beacon extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Seasia
 * @package        Seasia_Beacon
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Beacon admin controller
 *
 * @category    Seasia
 * @package     Seasia_Beacon
 * @author      Ultimate Module Creator
 */
class Seasia_Beacon_Adminhtml_Beacon_BeaconController extends Seasia_Beacon_Controller_Adminhtml_Beacon
{
    /**
     * init the beacon
     *
     * @access protected
     * @return Seasia_Beacon_Model_Beacon
     */
    protected function _initBeacon()
    {
        $beaconId  = (int) $this->getRequest()->getParam('id');
        $beacon    = Mage::getModel('seasia_beacon/beacon');
        if ($beaconId) {
            $beacon->load($beaconId);
        }
        Mage::register('current_beacon', $beacon);
        return $beacon;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('seasia_beacon')->__('Beacon'))
             ->_title(Mage::helper('seasia_beacon')->__('Beacon'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit beacon - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
    	
        $beaconId    = $this->getRequest()->getParam('id');
       
        $beacon      = $this->_initBeacon();
        if ($beaconId && !$beacon->getId()) {
            $this->_getSession()->addError(
                Mage::helper('seasia_beacon')->__('This beacon no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getBeaconData(true);
       
        
        if (!empty($data)) {
//         	echo "<pre>";
//         	print_r($beacon);
//         	die();
            $beacon->setData($data);
            //check if settings are added for the first time 
             
        }
        Mage::register('beacon_data', $beacon);
       
        $this->loadLayout();
        $this->_title(Mage::helper('seasia_beacon')->__('Beacon'))
             ->_title(Mage::helper('seasia_beacon')->__('Beacon'));
        if ($beacon->getId()) {
            $this->_title($beacon->getName());
        } else {
            $this->_title(Mage::helper('seasia_beacon')->__('Add beacon'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new beacon action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save beacon - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
		if ($data = $this->getRequest()->getPost('beacon')) 
        {
			try 
			{
				
				$beacon = $this->_initBeacon();
                
				$beacon->addData($data);
                //$beacon->setData('refresh_rate', $sched_time);
                $beacon->save();
                
                //check if customer is adding beacon for the first time
                if($beacon->getId())
                {
                	if(!$this->getRequest()->getParam('id'))
                	{
                		
                	$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
                 
                	$connection->beginTransaction();
                 
                	$select = $connection->select()
                	->from('seasia_beacon_settings', array('*'))
                	->where('vendor_id=?',$data['vendor']);
                	$rowsArray = $connection->fetchAll($select);
               
               		if(!empty($rowsArray[0]))
                	{
                		
	                	//update beacon table with range and refresh rate
	                	$__fields = array();
	                //	$__fields['proximity'] = $rowsArray[0]['beacon_range'];
	                	$__fields['refresh_rate'] = $rowsArray[0]['refresh_rate'];
	                	$__where = $connection->quoteInto('entity_id =?',$beacon->getId());
	                	$connection->update('seasia_beacon_beacon', $__fields, $__where);
	                	$connection->commit();
                	}
					else 
                	{
                		
                		//new entry in settings table,if new beacon of vednor is added
	                	$__fields = array();
	                	$__fields['beacon_range'] = $data['proximity'];
	                	$__fields['refresh_rate'] = $data['refresh_rate'];
	                	$__fields['vendor_id'] = $data['vendor'];
	                	$connection->insert('seasia_beacon_settings', $__fields);
	                	$connection->commit();
                	}
                }
                }
                //ends here
                
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('seasia_beacon')->__('Beacon was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $beacon->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setBeaconData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('seasia_beacon')->__('There was a problem saving the beacon.')
                );
                Mage::getSingleton('adminhtml/session')->setBeaconData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('seasia_beacon')->__('Unable to find beacon to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete beacon - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $beacon = Mage::getModel('seasia_beacon/beacon');
                $beacon->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('seasia_beacon')->__('Beacon was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('seasia_beacon')->__('There was an error deleting beacon.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('seasia_beacon')->__('Could not find beacon to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete beacon - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $beaconIds = $this->getRequest()->getParam('beacon');
        if (!is_array($beaconIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('seasia_beacon')->__('Please select beacon to delete.')
            );
        } else {
            try {
                foreach ($beaconIds as $beaconId) {
                    $beacon = Mage::getModel('seasia_beacon/beacon');
                    $beacon->setId($beaconId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('seasia_beacon')->__('Total of %d beacon were successfully deleted.', count($beaconIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('seasia_beacon')->__('There was an error deleting beacon.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $beaconIds = $this->getRequest()->getParam('beacon');
        if (!is_array($beaconIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('seasia_beacon')->__('Please select beacon.')
            );
        } else {
            try {
                foreach ($beaconIds as $beaconId) {
                $beacon = Mage::getSingleton('seasia_beacon/beacon')->load($beaconId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d beacon were successfully updated.', count($beaconIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('seasia_beacon')->__('There was an error updating beacon.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass Range change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massRangeAction()
    {
        $beaconIds = $this->getRequest()->getParam('beacon');
        if (!is_array($beaconIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('seasia_beacon')->__('Please select beacon.')
            );
        } else {
            try {
                foreach ($beaconIds as $beaconId) {
                $beacon = Mage::getSingleton('seasia_beacon/beacon')->load($beaconId)
                    ->setRange($this->getRequest()->getParam('flag_range'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d beacon were successfully updated.', count($beaconIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('seasia_beacon')->__('There was an error updating beacon.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'beacon.csv';
        $content    = $this->getLayout()->createBlock('seasia_beacon/adminhtml_beacon_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'beacon.xls';
        $content    = $this->getLayout()->createBlock('seasia_beacon/adminhtml_beacon_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'beacon.xml';
        $content    = $this->getLayout()->createBlock('seasia_beacon/adminhtml_beacon_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('seasia_beacon/beacon');
    }
    
    /*
     * Settings tab for beacons
     * 
     */
    public function beaconSettingsAction()
    {
    	if( $this->getRequest ()->isPost() )
    	{
    		$params = $this->getRequest()->getParams();
			
			if($params['vendor'] != "" && $params['range'] != "" && $params['refresh_rate'] != "")
			{	
    			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    	
    			$connection->beginTransaction();
    	
	    		$select = $connection->select()
	    		->from('seasia_beacon_settings', array('id'))
	    		->where('vendor_id=?',$params['vendor']);
	    		$rowsArray = $connection->fetchAll($select);
    	
    			if(!empty($rowsArray))
    			{
		    		$__fields = array();
		    		$__fields['beacon_range'] = $params['range'];
		    		$__fields['refresh_rate'] = $params['refresh_rate'];
		    		$__where = $connection->quoteInto('vendor_id =?',$params['vendor']);
		    		$connection->update('seasia_beacon_settings', $__fields, $__where);
		    		$connection->commit();
		    		
		    		//update beacons of particular vendor in seasia_beacon_beacon table
		    	
		    		$__fields = array();
		    		$__fields['proximity'] = $params['range'];
		    		$__fields['refresh_rate'] = $params['refresh_rate'];
		    		$__where = $connection->quoteInto('vendor =?',$params['vendor']);
		    		$connection->update('seasia_beacon_beacon', $__fields, $__where);
		    		$connection->commit();
		    		
		    		Mage::getSingleton('adminhtml/session')->addSuccess(
		    				Mage::helper('seasia_beacon')->__('Settings were successfully updated for all beacons'));
		    	}
    			else
    			{
	    			
		    		$__fields = array();
		    		$__fields['beacon_range'] = $params['range'];
		    		$__fields['refresh_rate'] = $params['refresh_rate'];
		    		$__fields['vendor_id'] = $params['vendor'];
		    		$connection->insert('seasia_beacon_settings', $__fields);
		    		$connection->commit();
		    		
		    		Mage::getSingleton('adminhtml/session')->addSuccess(
		    				Mage::helper('seasia_beacon')->__('Settings will be applied to your newly added beacons'));
    			}
			}
			else
			{
				 Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('seasia_beacon')->__('All fields are required'));
			}
    	}
    	$this->loadLayout();
    	$block = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'beacon.beacon_settings',
            array(
                'template' => 'beacon/beacon_settings.phtml'
            )
        );
		
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout();
    }
    
    public function settingsAction()
    {
    	
    	$vendor_id = $this->getRequest()->getParam('parent_id');
    	//check if settings already exists,to prepopulate in form
    	$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    	 
    	$connection->beginTransaction();
    	 
    	$select = $connection->select()
    	->from('seasia_beacon_settings', array('beacon_range','refresh_rate'))
    	->where('vendor_id=?',$vendor_id);
    	$rowsArray = $connection->fetchAll($select);
    	$connection->commit();
    	if($rowsArray)
    	{
    		echo json_encode($rowsArray[0]);
    		exit();
    	}
    	else
    	{
    		echo "false";
    		exit();
    	}
    	
    }
    
    public function assignPromocodeAction()
    {
    	if( $this->getRequest ()->isPost() )
    	{
    		$params = $this->getRequest()->getParams();
    		if($_FILES['promo_image'] != "")
    		{
    		
    			$imageName = "promoImg".mt_rand(10,1000)."_".$_FILES['promo_image']['name'];
    		}
    		else
    		{
    			$imageName = "";
    		}
//     		echo "<pre>";
//     		print_r($imageName);
//     		die();
    		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    		
    		$connection->beginTransaction();
    		
    		$__fields = array();
    		$__fields['beacon_id'] = $params['beacon'];
    		$__fields['promo_image'] = $imageName;
    		$__fields['promo_message'] = $params['message'];
    		$connection->insert('seasia_beacon_promocodes', $__fields);
    		
    		$connection->commit();
    		
    	}
    	
    	$this->loadLayout();
    	$block = $this->getLayout()->createBlock(
    			'Mage_Core_Block_Template',
    			'beacon.assign_promocode',
    			array(
    					'template' => 'beacon/assign_promocode.phtml'
    			)
    	);
    	
    	$this->getLayout()->getBlock('content')->append($block);
    	$this->renderLayout();
    	
    	
    }
}
