<?php
class Proximity_Beacon_Model_Mysql4_Beacon extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("beacon/beacon", "beacon_id");
    }
}