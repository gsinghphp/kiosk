<?php
/**
 * Seasia_Beacon extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Seasia
 * @package        Seasia_Beacon
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Beacon admin edit tabs
 *
 * @category    Seasia
 * @package     Seasia_Beacon
 * @author      Ultimate Module Creator
 */
class Seasia_Beacon_Block_Adminhtml_Beacon_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('beacon_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('seasia_beacon')->__('Beacon'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Seasia_Beacon_Block_Adminhtml_Beacon_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_beacon',
            array(
                'label'   => Mage::helper('seasia_beacon')->__('Beacon'),
                'title'   => Mage::helper('seasia_beacon')->__('Beacon'),
                'content' => $this->getLayout()->createBlock(
                    'seasia_beacon/adminhtml_beacon_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_beacon',
                array(
                    'label'   => Mage::helper('seasia_beacon')->__('Store views'),
                    'title'   => Mage::helper('seasia_beacon')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'seasia_beacon/adminhtml_beacon_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve beacon entity
     *
     * @access public
     * @return Seasia_Beacon_Model_Beacon
     * @author Ultimate Module Creator
     */
    public function getBeacon()
    {
        return Mage::registry('current_beacon');
    }
}
