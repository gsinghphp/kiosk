<?php

class Proximity_Category_Block_Adminhtml_Categorybackend_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        
        $this->_objectId = 'category_id';
        $this->_blockGroup = 'category';
        $this->_controller = 'adminhtml_categorybackend';
        parent::__construct();
        
        $this->_updateButton('save', 'label', 'Save Category');
        $this->_updateButton('delete', 'label', 'Delete Category');
    }

    public function getHeaderText() {
        return 'Create/Update Category';
    }

}
