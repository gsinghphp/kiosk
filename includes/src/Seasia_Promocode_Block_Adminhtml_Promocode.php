<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
class Seasia_Promocode_Block_Adminhtml_Promocode extends Mage_Adminhtml_Block_Widget_Grid_Container {

public function __construct() {
    $this->_controller = 'adminhtml_promocode';
    $this->_blockGroup = 'promocode';
    $this->_headerText = Mage::helper('promocode')->__('Manage Promocodes');
    $this->_addButtonLabel = Mage::helper('promocode')->__('Assign Promocodes');
    parent::__construct();
    //$this->_removeButton('add');
  }
  
}