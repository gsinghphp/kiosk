<?php
/**
 * Seasia_Beacon extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Seasia
 * @package        Seasia_Beacon
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Beacon admin edit form
 *
 * @category    Seasia
 * @package     Seasia_Beacon
 * @author      Ultimate Module Creator
 */
class Seasia_Beacon_Block_Adminhtml_Beacon_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'seasia_beacon';
        $this->_controller = 'adminhtml_beacon';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('seasia_beacon')->__('Save Beacon')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('seasia_beacon')->__('Delete Beacon')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('seasia_beacon')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_beacon') && Mage::registry('current_beacon')->getId()) {
            return Mage::helper('seasia_beacon')->__(
                "Edit Beacon '%s'",
                $this->escapeHtml(Mage::registry('current_beacon')->getName())
            );
        } else {
            return Mage::helper('seasia_beacon')->__('Add Beacon');
        }
    }
}
