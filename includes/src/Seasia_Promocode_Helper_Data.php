<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
class Seasia_Promocode_Helper_Data extends Mage_Core_Helper_Abstract {
	
	/**
	 * get all enabled beacons registered
	 * @author rkaur3
	 * version 1.0 
	 * 
	 */
	public function getAllEnabledBeacons()
	{
		$collection = Mage::getModel('seasia_beacon/beacon')->getCollection();
		$collection->getSelect()
		->reset(Zend_Db_Select::COLUMNS)
		->columns('entity_id as beacon_id')
		->columns('name as beacon_name');
		$collection->addFieldToFilter('status', array('eq'=>'1'));
		$beacons = $collection->getData();
		$beacon[] = array('value'=> "",'label'=>'Select Beacon');
		foreach($beacons as $data)
		{
			$beacon[] = array(
						'value' => $data['beacon_id'],
						'label' => Mage::helper('seasia_beacon')->__($data['beacon_name']),
						//'hidden' =>Mage::helper('seasia_beacon')->__($data['name'])
						);
		}
		return $beacon;
	}
	
	/**
	 * get generated coupons of all rules
	 * 
	 */
	public function getCoupons()
	{
		
		$collection = Mage::getResourceModel('salesrule/coupon_collection');

		$coupons_data = $collection->getData();
		$coupons[] = array('value'=> "",'label'=>'Select Coupon');
		foreach($coupons_data as $data)
		{
			$coupons[] = array(
					'value' => $data['coupon_id'],
					'label' => Mage::helper('seasia_beacon')->__($data['code']),
			);
		}
		return $coupons;
		
	}
	
	/**
	 * get temperature range
	 * 
	 */
	public function getTempRange($withEmpty = true, $defaultValues = false)
	{
		$options =  array(
				array(
						'label' => Mage::helper('seasia_beacon')->__('Select Temperature Range'),
						'value' => Mage::helper('seasia_beacon')->__(''),
				),
				array(
						'label' => Mage::helper('seasia_beacon')->__('between -20 and 0 Degrees'),
						'value' => Mage::helper('seasia_beacon')->__('-20and0'),
				),
				array(
						'label' => Mage::helper('seasia_beacon')->__('between 0 and 20 Degrees'),
						'value' => Mage::helper('seasia_beacon')->__('0and20'),
				),
				array(
						'label' => Mage::helper('seasia_beacon')->__('between 20and 40 Degrees'),
						'value' => Mage::helper('seasia_beacon')->__('20and40')
				),
	
				array(
						'label' => Mage::helper('seasia_beacon')->__('40 or above'),
						'value' => Mage::helper('seasia_beacon')->__('40and200')
				),
				 
		);
		
		return $options;
	
	}
}