<?php

class Proximity_Category_Model_Mysql4_Category extends Mage_Core_Model_Mysql4_Abstract {

    protected function _construct() {
        $this->_init("category/category", "category_id");
    }

}
