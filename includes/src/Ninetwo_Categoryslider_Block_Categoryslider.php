<?php
class Ninetwo_Categoryslider_Block_Categoryslider extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getCategoryslider()     
     { 
        if (!$this->hasData('categoryslider')) {
            $this->setData('categoryslider', Mage::registry('categoryslider'));
        }
        return $this->getData('categoryslider');
        
    }
}