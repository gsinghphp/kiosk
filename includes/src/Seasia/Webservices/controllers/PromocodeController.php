<?php
class Seasia_Webservices_PromocodeController extends Mage_Core_Controller_Front_Action
{
	public function getpromocodeAction()
	{
		$handle = fopen('php://input', 'r' );
    	$jsonInput = fgets ( $handle );
    	$params = Zend_Json::decode ( $jsonInput );
		
		$beacon_array = array();
		
    	for($i=0;$i<count($params['beacon']);$i++)
    	{
			//get beacon id and its data
	    	$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
	    	
	    	$connection->beginTransaction();
	    	  
	    	$select = $connection->select()
	    	->from('seasia_beacon_beacon', array('*'))
	    	->where('uuid=?',$params['beacon'][$i]['uuid'])
	    	->where('proximity=?',$params['beacon'][$i]['proximity'])
	    	->where('major=?',$params['beacon'][$i]['major'])
	    	->where('minor=?',$params['beacon'][$i]['minor']);
	    	
	    	$connection->commit();
	    	$rowsArray = $connection->fetchAll($select);
	    	 
	    	 
    		$bData = $rowsArray[0];
			
    		if($bData['entity_id'] != "")
    		{
	    		$beacon_array[$i]['beacon_id'] = $bData['entity_id'];
	    		$beacon_array[$i]['beacon_name'] = $bData['name'];
	    		$beacon_array[$i]['uuid'] = $bData['uuid'];
	    		$beacon_array[$i]['major'] = $bData['major'];
	    		$beacon_array[$i]['minor'] = $bData['minor'];
	    		$beacon_array[$i]['range'] = $bData['proximity'];
    			
	    		$beacon_array[$i]['beacon_id'] = $bData['entity_id'];
	    		$beacon_array[$i]['beacon_name'] = $bData['name'];
	    		$beacon_array[$i]['uuid'] = $bData['uuid'];
	    		$beacon_array[$i]['major'] = $bData['major'];
	    		$beacon_array[$i]['minor'] = $bData['minor'];
	    		$beacon_array[$i]['range'] = $bData['proximity'];
    			
    			/*code for push notification starts from here*/
    			require_once('Seasia/Webservices/controllers/IndexController.php');
    			$controller         = new Seasia_Webservices_IndexController(
    					Mage::app()->getRequest(),
    					Mage::app()->getResponse()
    			);
    			//call the action
    			
    			$controller->manageFrequencyAction($beacon_array[$i]['beacon_id'],$params['user_id'],$params['client_time']);
				/*code for push notification ends here*/
    		
    			//get coupon data of beacon
    			$select = $connection->select()
    			->from('seasia_beacon_promocodes', array('*'))
    			->where('beacon_id=?',$beacon_array[$i]['beacon_id']);
    			$connection->commit();
    			$promArray = $connection->fetchAll($select);
				
				/* get coupon details according to coupon id*/
				$collection = Mage::getResourceModel('salesrule/coupon_collection');
    			$collection->addFieldToFilter('coupon_id',array('eq'=>$promArray[0]['coupon_id']));
    			$cData = $collection->getData();
    			
    			
    			$rule = Mage::getModel('salesrule/rule')->load($cData[0]['rule_id']);
    			
    			
    			$beacon_array[$i]['discount'] = $rule->getDiscountAmount();
    			$beacon_array[$i]['validity_date'] = $rule->getTo_date();
    			$beacon_array[$i]['passlink'] = $cData[0]['passlink'];
    			$beacon_array[$i]['coupon_id'] = $promArray[0]['coupon_id'];
    			$beacon_array[$i]['coupon_code'] = $cData[0]['code'];
		    	$beacon_array[$i]['message'] = $promArray[0]['promo_message'];
		    	
		    	if($promArray[0]['promo_image'] != "")
		    	{
		    		$beacon_array[$i]['image'] = $promo_image_path =  Mage::getBaseUrl('media')."promo_images/". $promArray[0]['promo_image'];
		    	}
		    	else
		    	{
		    		$beacon_array[$i]['image'] = "No image";
		    	}
			}
		}
    	$result = array("Status"=>200,"beacon_promocode_data"=>$beacon_array);
    	echo json_encode($result);
    	exit();
	} 
	
	public function createPassbookAction()
	{
		$root_path = Mage::getBaseDir('lib');
		$image_path = Mage::getBaseDir('skin')."/adminhtml/default/default/images/passbook_images/";
		$TempPath = Mage::getBaseDir()."/Passes";
		
		//generate pass.json for coupon 
		require_once(Mage::getBaseDir('lib') . '/Passbook/PassBookPass.php');
		$obj2 = new PassBook\Pass(COUPON,"pass.com.GeoTrack","HE7TDTH6H9","Beacon General Store","Beacon General Store Coupon","Super Store","rgb(22, 55, 110)","rgb(50, 91, 185)");
		$obj2->addBarCode("SGSC00145UP");
		
		$obj2->addLocation("27.43903","69.4532322");
		$obj2->addLocation("88.85342","33.7540967");
		$obj2->couponPass_addHeaderInfo("No","6901");
		$obj2->couponPass_addDesc("on all mobile phones","5% Off");
		$obj2->couponPass_addInfo("Expiry:","04/26/2014");
		$obj2->couponPass_addInfo("Member:","John");
		$obj2->addIcon($image_path."icon.png");
		$obj2->addIconHD($image_path."icon@2x.png");
		$obj2->addLogo($image_path."logo.png");
		$obj2->addLogoHD($image_path."logo@2x.png");
		$obj2->addBackScreenInfo("Copyrights","All rights reserved Super Beacon Store");
		
		$JSON = $obj2->getpassData();
		
		
		//GENERATE PKPASS FILE 
		require_once(Mage::getBaseDir('lib') . '/Passbook/passkit.php');
		$wwdr_pem_file = $root_path."/certificates/WWDR.pem";
		$passbook_cert = $root_path."/certificates/PassbookCert.p12";
		
		$Certificates = array('AppleWWDRCA'  => $wwdr_pem_file,
				'Certificate'  => $passbook_cert,
				'CertPassword' => 'seasia@123');
		
		
		$ImageFiles = array($image_path.'icon.png', $image_path.'icon@2x.png', $image_path.'logo.png');
		
		//$JSON = file_get_contents($root_path."/Passbook/pass.json");
		
		$pkPass_File_Link = echoPass(createPass($Certificates, $ImageFiles, $JSON, 'couponPass', $TempPath));
	}
	
	/**
	 * add fav coupons of user
	 * 
	 * @author rkaur3
	 * @version 1.0
	 */
	public function addFavCouponAction()
	{
		$handle = fopen('php://input', 'r' );
		$jsonInput = fgets ( $handle );
		$params = Zend_Json::decode ( $jsonInput );
		
		if($params['user_id'] != "" && $params['coupon_id'] != "")
		{
			//check if coupon already added in favs
			
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			$connection->beginTransaction();
			
			$select = $connection->select()
			->from('coupon_favs_user', array('*'))
			->where('user_id=?',$params['user_id'])
			->where('coupon_id=?',$params['coupon_id']);
			$rowsArray = $connection->fetchAll($select);
			
			if(!empty($rowsArray))
			{
				$result = array("Status"=>301,"message"=>"Coupon is already in your favorites");
			}
			else
			{
				$__fields = array();
				$__fields['user_id'] = $params['user_id'];
				$__fields['coupon_id'] = $params['coupon_id'];
				$connection->insert('coupon_favs_user', $__fields);
				
				$result = array("Status"=>200,"message"=>"Coupon added to favorites successfully");
			}
			
			$connection->commit();
		}
		else
		{
			$result = array("Status"=>301,"Error"=>"Missing parameters");
		}
		$data = Zend_Json::encode($result);
		echo $data;
		exit();
	}
	
	/**
	 * Delete fav coupon of user 
	 * 
	 * @author rkaur3
	 * @version 1.0
	 */
	public function delFavCouponAction()
	{
		$handle = fopen('php://input', 'r' );
		$jsonInput = fgets ( $handle );
		$params = Zend_Json::decode ( $jsonInput );
		
		if($params['user_id'] != "" && $params['coupon_id'] != "")
		{
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			$connection->beginTransaction();
			$__condition = 'user_id ='. $params['user_id'].' AND coupon_id='.$params['coupon_id'];
			$connection->delete('coupon_favs_user', $__condition);
			$connection->commit();
		    
			$result = array("Status"=>200,"message"=>"Coupon removed from favorites successfully");
		}
		else
		{
			$result = array("Status"=>301,"Error"=>"Missing parameters");
		}
		$data = Zend_Json::encode($result);
		echo $data;
		exit();
	}
	
	/**
	 * get (fav)coupon details of user
	 * @author rkaur3
	 * @version 1.0
	 */
	
	public function getFavCouponsAction()
	{
		$handle = fopen('php://input', 'r' );
		$jsonInput = fgets ( $handle );
		$params = Zend_Json::decode ( $jsonInput );
		
		/*if($params['user_id'] != "")
		{
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			 
			$connection->beginTransaction();
			 
			$select = $connection->select()
			->from('coupon_favs_user', array('coupon_id'))
			->where('user_id=?',$params['user_id']);
			$rowsArray = $connection->fetchAll($select);

			$coupon_array = array();
			$i = 0;
			foreach($rowsArray as $coupon)
			{
				
				 get coupon details according to coupon id
				$collection = Mage::getResourceModel('salesrule/coupon_collection');
				$collection->addFieldToFilter('coupon_id',array('eq'=>$coupon['coupon_id']));
				$cData = $collection->getData();
				$coupon_array[$i] = $cData[0];
		
				$rule = Mage::getModel('salesrule/rule')->load($cData[0]['rule_id']);
    			$coupon_array[$i]['discount'] = $rule->getDiscountAmount();
    			$i++;
			}
			if(!empty($coupon_array))
			{
				$result = array("Status"=>200,"message"=>"Success","couponData"=>$coupon_array);
			}
			else
			{
				$result = array("Status"=>301,"Error"=>"No Data Found");
			}
		}
		else
		{
			$result = array("Status"=>301,"Error"=>"Missing parameters");
		}
		$data = Zend_Json::encode($result);
		echo $data;
		exit();
		*/
		if($params['user_id'] != "")
		{
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		
			$connection->beginTransaction();
		
			$select = $connection->select()
			->from('coupon_favs_user', array('coupon_id'))
			->where('user_id=?',$params['user_id']);
			$rowsArray = $connection->fetchAll($select);
		
			$coupon_array = array();
			$i=0;
			foreach($rowsArray as $key=> $coupon)
			{
		
				/* get coupon details according to coupon id*/
				$collection = Mage::getResourceModel('salesrule/coupon_collection');
				$collection->addFieldToFilter('coupon_id',array('eq'=>$coupon['coupon_id']));
				$cData = $collection->getData();
				//0 if coupon exists,1 if coupon removed
				if(!empty($cData[0]))
				{
					$coupon_array[$i] = $cData[0];
		
					$rule = Mage::getModel('salesrule/rule')->load($cData[0]['rule_id']);
					$coupon_array[$i]['discount'] = $rule->getDiscountAmount();
					$coupon_array[$i]['coupon_status'] = 0;
				}
				else
				{
					$coupon_array[$i]['coupon_id'] = $coupon['coupon_id'];
					$coupon_array[$i]['coupon_status'] = 1;
				}
				$i++;
			}
			if(!empty($coupon_array))
			{
				$result = array("Status"=>200,"message"=>"Success","couponData"=>$coupon_array);
			}
			else
			{
				$result = array("Status"=>301,"Error"=>"No Data Found");
			}
		}
		else
		{
			$result = array("Status"=>301,"Error"=>"Missing parameters");
		}
		$data = Zend_Json::encode($result);
		echo $data;
		exit();
		
		
	}
	
	public function testAction()
	{
		
		$handle = fopen('php://input', 'r' );
		$jsonInput = fgets ( $handle );
		$params = Zend_Json::decode ( $jsonInput );
		

		
		$temp = 45;
		
		$beacon_array = array();
		
		for($i=0;$i<count($params['beacon']);$i++)
		{
			//get beacon id and its data
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		
			$connection->beginTransaction();
		
			$select = $connection->select()
			->from('seasia_beacon_beacon', array('*'))
			->where('uuid=?',$params['beacon'][$i]['uuid'])
			->where('proximity=?',$params['beacon'][$i]['proximity'])
			->where('major=?',$params['beacon'][$i]['major'])
					->where('minor=?',$params['beacon'][$i]['minor']);
					$connection->commit();
					$rowsArray = $connection->fetchAll($select);
					$bData = $rowsArray[0];
				
			if($bData['entity_id'] != "")
			{
				$beacon_array[$i]['beacon_id'] = 80;
				$beacon_array[$i]['beacon_name'] = $bData['name'];
				$beacon_array[$i]['uuid'] = $bData['uuid'];
				$beacon_array[$i]['major'] = $bData['major'];
				$beacon_array[$i]['minor'] = $bData['minor'];
				$beacon_array[$i]['range'] = $bData['proximity'];
				 
				$beacon_array[$i]['beacon_id'] = $bData['entity_id'];
				$beacon_array[$i]['beacon_name'] = $bData['name'];
				$beacon_array[$i]['uuid'] = $bData['uuid'];
				$beacon_array[$i]['major'] = $bData['major'];
				$beacon_array[$i]['minor'] = $bData['minor'];
				$beacon_array[$i]['range'] = $bData['proximity'];
				 
				/*code for push notification starts from here*/
				/*require_once('Seasia/Webservices/controllers/IndexController.php');
					$controller         = new Seasia_Webservices_IndexController(
					Mage::app()->getRequest(),
					Mage::app()->getResponse()
					);
				
				//call the action
				$controller->manageFrequencyAction($beacon_array[$i]['beacon_id'],$params['user_id'],$params['client_time']);
				/*code for push notification ends here*/
		//get coupon data of beacon
				$select = $connection->select()
				->from('seasia_beacon_promocodes', array('*'))
				->where('beacon_id=?',$beacon_array[$i]['beacon_id'])
				->where('min_temp <'.$temp)
				->where('max_temp >'.$temp);
				$connection->commit();
				 $promArray = $connection->fetchAll($select);
		

				 //check if any coupon is assigned to beacon
				 if(!empty($promArray[0]))
				 {	
					/* get coupon details according to coupon id*/
					$collection = Mage::getResourceModel('salesrule/coupon_collection');
					$collection->addFieldToFilter('coupon_id',array('eq'=>$promArray[0]['coupon_id']));
					$cData = $collection->getData();
					 
					 
					$rule = Mage::getModel('salesrule/rule')->load($cData[0]['rule_id']);

					$beacon_array[$i]['coupon_assigned'] = "Yes";
					$beacon_array[$i]['discount'] = $rule->getDiscountAmount();
					$beacon_array[$i]['passlink'] = $cData[0]['passlink'];
					$beacon_array[$i]['coupon_id'] = $promArray[0]['coupon_id'];
					$beacon_array[$i]['coupon_code'] = $cData[0]['code'];
					$beacon_array[$i]['message'] = $promArray[0]['promo_message'];
				 
					if($promArray[0]['promo_image'] != "")
					{
						$beacon_array[$i]['image'] = $promo_image_path =  Mage::getBaseUrl('media')."promo_images/". $promArray[0]['promo_image'];
					}
					else
					{
						$beacon_array[$i]['image'] = "No image";
					}
				 }
				 else
				 {
				 	$beacon_array[$i]['coupon_assigned'] = "No";
				 }
			}
		}
		$result = array("Status"=>200,"beacon_promocode_data"=>$beacon_array);
		echo json_encode($result);
		exit();
	}
}
?>
