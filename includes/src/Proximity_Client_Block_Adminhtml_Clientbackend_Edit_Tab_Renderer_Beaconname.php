<?php

class Proximity_Client_Block_Adminhtml_Clientbackend_Edit_Tab_Renderer_Beaconname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
//echo $row->getData('beacon_id');exit;
        return Mage::getModel('beacon/beacon')->getBeaconName($row->getData('beacon_id'));
    }

}
