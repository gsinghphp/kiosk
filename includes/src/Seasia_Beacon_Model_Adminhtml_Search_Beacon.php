<?php
/**
 * Seasia_Beacon extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Seasia
 * @package        Seasia_Beacon
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    Seasia
 * @package     Seasia_Beacon
 * @author      Ultimate Module Creator
 */
class Seasia_Beacon_Model_Adminhtml_Search_Beacon extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Seasia_Beacon_Model_Adminhtml_Search_Beacon
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('seasia_beacon/beacon_collection')
            ->addFieldToFilter('name', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $beacon) {
            $arr[] = array(
                'id'          => 'beacon/1/'.$beacon->getId(),
                'type'        => Mage::helper('seasia_beacon')->__('Beacon'),
                'name'        => $beacon->getName(),
                'description' => $beacon->getName(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/beacon_beacon/edit',
                    array('id'=>$beacon->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
