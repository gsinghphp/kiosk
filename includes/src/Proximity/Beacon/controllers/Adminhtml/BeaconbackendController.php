<?php

class Proximity_Beacon_Adminhtml_BeaconbackendController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->_title($this->__("Manage Beacon"));
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $testId = $this->getRequest()->getParam('beacon_id');
        $beaconModel = Mage::getModel('beacon/beacon')->load($testId);
        if ($beaconModel->getId() || $testId == 0) {
            Mage::register('beacon_data', $beaconModel);
            $this->loadLayout();
            $this->getLayout()->getBlock('head')
                    ->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()
                            ->createBlock('beacon/adminhtml_beaconbackend_edit'))
                    ->_addLeft($this->getLayout()
                            ->createBlock('beacon/adminhtml_beaconbackend_edit_tabs')
            );
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')
                    ->addError('Beacon does not exist');
            $this->_redirect('*/*/');
        }
    }

    public function saveAction() {
        if ($this->getRequest()->getPost()) {
            try {
                $postData = $this->getRequest()->getPost();
                $beaconModel = Mage::getModel('beacon/beacon');
                if ($this->getRequest()->getParam('beacon_id') <= 0)
                    $beaconModel->setCreatedTime(
                            Mage::getSingleton('core/date')
                                    ->gmtDate()
                    );
                $beaconModel
                        ->addData($postData)
                        ->setUpdateTime(
                                Mage::getSingleton('core/date')
                                ->gmtDate())
                        ->setId($this->getRequest()->getParam('beacon_id'))
                        ->save();
                $lastBeaconId = $beaconModel->getId();
                if (isset($postData['page_id']) && !empty($postData['page_id'])) {
                    $this->createUpdateBeaconPage($postData, null, $postData['page_id']);
                } else {
                    $this->createUpdateBeaconPage($postData, $lastBeaconId);
                }

                Mage::getSingleton('adminhtml/session')
                        ->addSuccess('Beacon Information Successfully Saved');
                Mage::getSingleton('adminhtml/session')
                        ->settestData(false);
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')
                        ->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')
                        ->settestData($this->getRequest()
                                ->getPost()
                );
                $this->_redirect('*/*/edit', array('beacon_id' => $this->getRequest()
                            ->getParam('beacon_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('beacon_id') > 0) {
            try {
                $clientModel = Mage::getModel('beacon/beacon');
                $pageId = $clientModel->load($this->getRequest()->getParam('beacon_id'))->getPageId();
                
                $clientModel->setId($this->getRequest()
                                ->getParam('beacon_id'))
                        ->delete();
                Mage::getModel('cms/page')->setId($pageId)->delete();
                Mage::getSingleton('adminhtml/session')
                        ->addSuccess('Beacon Successfully Deleted');
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')
                        ->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('beacon_id' => $this->getRequest()->getParam('beacon_id')));
            }
        }
        $this->_redirect('*/*/');
    }

    protected function createUpdateBeaconPage($postData, $beaconId = null, $pageId = null) {
        if (is_null($pageId)) {
            // Creating cms page for beacon call
            $cmsPageData = array(
                'title' => $postData['beacon_name'],
                'root_template' => 'empty',
                'meta_keywords' => $postData['beacon_name'],
                'meta_description' => $postData['beacon_name'],
                'identifier' => $postData['url'],
                'content_heading' => '',
                'stores' => array(0), //available for all store views
                'content' => $postData['message'],
            );

            $cmsModel = Mage::getModel('cms/page')->setData($cmsPageData)->save();
            $lastCmsPageId = $cmsModel->getId();
            $data = array(
                "page_id" => $lastCmsPageId,
            );

            $model = Mage::getModel('beacon/beacon')->load($beaconId)->addData($data);
            $model->setId($beaconId)->save();
        } else {
            $cmsModel = Mage::getModel('cms/page')->load($postData['url'], 'identifier');
            $cmsPageData = array(
                'title' => $postData['beacon_name'],
                'root_template' => 'empty',
                'meta_keywords' => $postData['beacon_name'],
                'meta_description' => $postData['beacon_name'],
                'identifier' => $postData['url'],
                'content_heading' => '',
                'stores' => array(0), //available for all store views
                'content' => $postData['message'],
            );

            $cmsModel->setData($cmsPageData);
            $cmsModel->setId($pageId)->save();
        }
    }

}
