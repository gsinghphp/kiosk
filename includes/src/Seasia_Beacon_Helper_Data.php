<?php
/**
 * Seasia_Beacon extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Seasia
 * @package        Seasia_Beacon
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Beacon default helper
 *
 * @category    Seasia
 * @package     Seasia_Beacon
 * @author      Ultimate Module Creator
 */
class Seasia_Beacon_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * convert array to options
     *
     * @access public
     * @param $options
     * @return array
     * @author Ultimate Module Creator
     */
    public function convertOptions($options)
    {
        $converted = array();
        foreach ($options as $option) {
            if (isset($option['value']) && !is_array($option['value']) &&
                isset($option['label']) && !is_array($option['label'])) {
                $converted[$option['value']] = $option['label'];
            }
        }
        return $converted;
    }
    
    public function getAllCategoriesArray($optionList = false)
    {
    	$categoriesArray = Mage::getModel('catalog/category')
    	->getCollection()
    	->addAttributeToSelect('name')
    	->addAttributeToSelect('url_path')
    	->addAttributeToSort('path', 'asc')
    	->addFieldToFilter('is_active', array('eq'=>'1'))
    	->load()
    	->toArray();
    
    	if (!$optionList) {
    		return $categoriesArray;
    	}
    	
    	$categories[] = array('value'=> "",
    			'label'=>'Please Select Category'
    			);
    	foreach ($categoriesArray as $categoryId => $category) {

    		if (isset($category['name'])) {
    			$categories[] = array(
    					'value' => $category['entity_id'],
    					//'value' => $category['name'],
    					'label' => Mage::helper('seasia_beacon')->__($category['name'])
    			);
    		}
    	}
    
    	return $categories;
    }
     
    /**
     * get all active & approved sellers/vendors
     * 
     * @author rkaur3
     */
    public function getAllVendors()
    {
    	
    	$collectionfetch = Mage::getModel('marketplace/userprofile')->getCollection();
    	$record = array();
    	foreach($collectionfetch as $id)
    	{
    		$record[] = $id->getmageuserid();
    	}

    	if(count($record)!=0){
    		$collection = Mage::getModel('customer/customer')->getCollection()
    		->addNameToSelect()
    		->addAttributeToSelect('email');
    		$collection->addAttributeToFilter('entity_id', array('in' => $record));
    		
    		$collection->joinTable('marketplace/userprofile', 'mageuserid=entity_id', array('*'), null, 'left');
   		
    	}
    	
    	else{
    		$collectionfetch->addFieldToFilter('mageuserid', array('eq' => -1));
    	}
    	$collectionData = $collection->getData();

    	$vendor[] = array('value'=> "",
    			'label'=>'Select Vendor'
    			);
    	foreach($collectionData as $data)
    	{
    		//check if vendor is approved
    		if ($data['partnerstatus'] == "Seller") {
    			$vendor[] = array(
    					//'value' => $data['entity_id'],
    					'value' => $data['entity_id'],
    					'label' => Mage::helper('seasia_beacon')->__($data['name']),
    					'hidden' =>Mage::helper('seasia_beacon')->__($data['name'])
    				
    			);
    		}
    	}

		return $vendor;
    	
    	
    }
}
