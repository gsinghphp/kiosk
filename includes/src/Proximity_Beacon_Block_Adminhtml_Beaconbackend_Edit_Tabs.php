<?php

class Proximity_Beacon_Block_Adminhtml_Beaconbackend_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {
    
    public function _construct() {
        
        parent::_construct();
        
        $this->setId('beacon_tabs');
        $this->setDestElementId('edit_form');
        
    }
    
    public function _beforeToHtml() {
        
        $this->addTab('form_section', array(
            'label' => 'Beacon information',
            'title' => 'Beacon information',
            'content' => $this->getLayout()
                    ->createBlock('beacon/adminhtml_beaconbackend_edit_tab_form')
                    ->toHtml()
        ));
        
        return parent::_beforeToHtml();
    }
}
