<?php

class Proximity_Beacon_Block_Adminhtml_Beaconbackend_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    public function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('beacon_form', array('legend' => 'beacon information'));

        $fieldset->addField('beacon_name', 'text', array(
            'label' => 'Beacon Name',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'beacon_name',
        ));
        $fieldset->addField("description", "editor", array(
            "label" => Mage::helper("beacon")->__("Beacon Description"),
            "class" => "required-entry",
            "required" => true,
            "style" => "height:15em",
            "name" => "description",
            "config" => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            "wysiwyg" => true,
        ));
        $fieldset->addField('beacon_number', 'text', array(
            'label' => 'Beacon Number',
            'class' => 'validate-number',
            'required' => true,
            'name' => 'beacon_number',
        ));
        $fieldset->addField('uid', 'text', array(
            'label' => 'Beacon UID',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'uid',
        ));
        $fieldset->addField('major_number', 'text', array(
            'label' => 'Major Number',
            'class' => 'validate-number',
            'required' => true,
            'name' => 'major_number',
        ));
        $fieldset->addField('minor_number', 'text', array(
            'label' => 'Minor Number',
            'class' => 'validate-number',
            'required' => true,
            'name' => 'minor_number',
        ));
        $fieldset->addField('proximity', 'text', array(
            'label' => 'Proximity',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'proximity',
        ));
        $fieldset->addField('refresh_rate', 'text', array(
            'label' => 'Referesh Rate',
            'class' => 'validate-number',
            'required' => true,
            'name' => 'refresh_rate',
        ));
        $fieldset->addField("message", "editor", array(
            "label" => Mage::helper("beacon")->__("Beacon Message"),
            "class" => "required-entry",
            "required" => true,
            "style" => "height:15em",
            "name" => "message",
            "config" => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            "wysiwyg" => true,
        ));
        $fieldset->addField('url', 'text', array(
            'label' => 'Beacon URL',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'url',
        ));
        $fieldset->addField('status', 'select', array(
            'name' => 'status',
            'label' => 'Beacon Staus',
            'id' => 'status',
            'title' => 'Beacon Status',
            'required' => true,
            'values' => array('1' => 'Enable', '0' => 'Disable'),
        ));
        $fieldset->addField('page_id', 'hidden', array(
            'name' => 'page_id',
            'id' => 'page_id',
            'required' => false,
        ));

        if (Mage::registry('beacon_data')) {
            $form->setValues(Mage::registry('beacon_data')->getData());
        }
        return parent::_prepareForm();
    }

}
